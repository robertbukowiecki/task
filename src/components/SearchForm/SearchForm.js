import React, { useRef } from "react";

import styles from "./SearchForm";

function SearchForm(props) {
  const inputEl = useRef("");

  const getSearch = (event) => {
    props.handleChange(inputEl.current.value);
    event.preventDefault();
  };

  return (
    <div className={styles.component}>
      <div>
        <form>
          <label>search</label>
          <input
            ref={inputEl}
            type="text"
            onChange={getSearch}
            value={props.search}
          ></input>
        </form>
      </div>
    </div>
  );
}

export default SearchForm;
