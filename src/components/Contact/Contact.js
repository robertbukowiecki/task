import React, { useState } from "react";

import styles from "./Contact.scss";
import { Row, Col } from "react-bootstrap";

function Contact(props) {
  const [contactChecked, setContactChecked] = useState(false);
  const clickHandler = () => {
    props.active(props.id);
    if (!contactChecked) {
      setContactChecked(true);
    } else {
      setContactChecked(false);
    }
  };

  const { avatar, first_name, last_name, email } = props;
  return (
    <li onClick={clickHandler}>
      <Row>
        <Col xs={1}>
          <div className={styles.avatar}>
            <img src={avatar} alt="avatar"></img>
          </div>
        </Col>
        <Col xs={4}>
          <div className={styles.name}>
            {first_name} {last_name}
          </div>
          <div className={styles.user}>{email}</div>
        </Col>
        <div className={styles.checkbox}>
          <input
            type="checkbox"
            checked={contactChecked}
            onChange={() => contactChecked}
          ></input>
        </div>
      </Row>
    </li>
  );
}

export default Contact;
