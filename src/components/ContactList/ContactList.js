import React, { useState } from "react";
import Contact from "../Contact/Contact";

import styles from "./ContactList.scss";

function ContactList(props) {
  const [activedContacts, setActivedContacts] = useState([]);

  const activedContactHandler = (id) => {
    const updatedActivedContacts = [];
    updatedActivedContacts.push(id);
    setActivedContacts(updatedActivedContacts);
    console.log(updatedActivedContacts);
  };
  const { contacts } = props;
  return (
    <ul className={styles.component}>
      {contacts.map((contact) => (
        <Contact active={activedContactHandler} key={contact.id} {...contact} />
      ))}
    </ul>
  );
}

export default ContactList;
