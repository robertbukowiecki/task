import React, { useEffect, useState } from "react";
import "./App.scss";
import ContactList from "./components/ContactList/ContactList";
import PageTitle from "./components/PageTitle/PageTitle";
import SearchForm from "./components/SearchForm/SearchForm";
import { Container } from "react-bootstrap";

function App() {
  const [contacts, setContacts] = useState([]);
  const [searchString, setSearchString] = useState("");
  const [searchResult, setSearchResult] = useState([]);
  const [activeContacts, setActiveContact] = useState([]);

  function dynamicSort(property) {
    let sortOrder = 1;

    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }

    return function (a, b) {
      if (sortOrder === -1) {
        return b[property].localeCompare(a[property]);
      } else {
        return a[property].localeCompare(b[property]);
      }
    };
  }

  useEffect(() => {
    fetch(
      "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    )
      .then((res) => res.json())
      .then((res) => {
        setContacts(res.sort(dynamicSort("last_name")));
        setSearchResult(res.sort(dynamicSort("last_name")));
      });
  }, []);

  const searchHandler = (searchString) => {
    setSearchString(searchString);
    if (!searchString.length) {
      setSearchResult(contacts);
    } else {
      const newContactList = contacts.filter((contact) => {
        return (
          contact.first_name
            .toLowerCase()
            .search(searchString.toLowerCase()) !== -1 ||
          contact.last_name.toLowerCase().search(searchString.toLowerCase()) !==
            -1
        );
      });
      setSearchResult(newContactList);
    }
  };

  return (
    <div>
      <Container>
        <PageTitle title="contacts" />
        <SearchForm handleChange={searchHandler} search={searchString} />
        <ContactList contacts={searchResult}></ContactList>
      </Container>
    </div>
  );
}

export default App;
